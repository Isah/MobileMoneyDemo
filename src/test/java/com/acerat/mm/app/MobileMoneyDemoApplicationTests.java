package com.acerat.mm.app;


import com.acerat.mm.app.aggregate.AccountAggregate;
import com.acerat.mm.app.commands.CreateAccountCommand;
import com.acerat.mm.app.commands.DepositMoneyCommand;
import com.acerat.mm.app.errors.DepositException;
import com.acerat.mm.app.errors.DepositExceptionMaxAmount;
import com.acerat.mm.app.errors.DepositExceptionMinimumAmount;
import com.acerat.mm.app.events.AccountCreatedEvent;
import com.acerat.mm.app.events.AccountDepositEvent;
import org.axonframework.test.aggregate.AggregateTestFixture;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.axonframework.test.aggregate.FixtureConfiguration;


class MobileMoneyDemoApplicationTests {

	/*
	Specialized versions of the event bus,
	 command bus and event store are provided as part of the fixture by default.
	 */

	private FixtureConfiguration<AccountAggregate> fixture;

	@BeforeEach
	public void setUp() {
		//CommandBus commandBus = SimpleCommandBus.builder().build();
		fixture = new AggregateTestFixture<>(AccountAggregate.class);
		// Adjusted - Used builder instead of constructor
	}

	@Test
	public void createAccount() {

		CreateAccountCommand c = new CreateAccountCommand();
		c.setAccountId("1");
		c.setBalance(100);


		AccountCreatedEvent event = new AccountCreatedEvent();
		event.setAccountId("1");
		event.setBalance(100);
		event.setStatus("CREATED");

		fixture.given()
				.when(c)
				.expectSuccessfulHandlerExecution()
				.expectEvents(event);


	}


	@Test
	public void depositInitialMoney() {

		AccountCreatedEvent event = new AccountCreatedEvent();
		event.setAccountId("1");
		event.setStatus("CREATED");

		DepositMoneyCommand deposit = new DepositMoneyCommand();
		deposit.setAccountId("1");
		deposit.setAmount(200.00);

		fixture.given(event)
				.when(deposit)
				.expectSuccessfulHandlerExecution()
				.expectEvents(new AccountDepositEvent("1", 200.0));
	}


	@Test
	public void depositMoney() {

		AccountDepositEvent event = new AccountDepositEvent();
		event.setAccountId("1");
		event.setAmount(100);

		DepositMoneyCommand deposit = new DepositMoneyCommand();
		deposit.setAccountId("1");
		deposit.setAmount(200.00);

		fixture.given(event)
				.when(deposit)
				.expectSuccessfulHandlerExecution()
				.expectEvents(new AccountDepositEvent("1", 200.0));
	}

	@Test
	public void failToDepositIfMoneyIsInsufficient() {

		AccountCreatedEvent event = new AccountCreatedEvent();
		event.setAccountId("1");
		DepositMoneyCommand deposit = new DepositMoneyCommand();
		deposit.setAccountId("1");
		deposit.setAmount(499.0);
		fixture.given(event)
				.when(deposit)
				.expectException(DepositExceptionMinimumAmount.class);
	}


	@Test
	public void failToDepositIfMoneyExceedsMax() {

		AccountCreatedEvent event = new AccountCreatedEvent();
		event.setAccountId("1");
		DepositMoneyCommand deposit = new DepositMoneyCommand();
		deposit.setAccountId("1");
		deposit.setAmount(4999999.0);
		fixture.given(event)
				.when(deposit)
				.expectException(DepositExceptionMaxAmount.class);
	}


}

