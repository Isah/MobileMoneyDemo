package com.acerat.mm;

import com.acerat.mm.app.errors.ExceptionWrappingHandlerInterceptor;
import org.axonframework.springboot.autoconfig.JdbcAutoConfiguration;
import org.axonframework.springboot.autoconfig.JpaEventStoreAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication
public class MobileMoneyDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MobileMoneyDemoApplication.class, args);
	}


	@Bean
	@Profile("command")
	public ExceptionWrappingHandlerInterceptor exceptionWrappingHandlerInterceptor() {
		return new ExceptionWrappingHandlerInterceptor();
	}


}
