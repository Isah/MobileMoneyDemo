package com.acerat.mm.app.commands;

import lombok.Builder;
import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
public class DepositMoneyCommand {

    @TargetAggregateIdentifier
    private String accountId;
    private double amount;


}
