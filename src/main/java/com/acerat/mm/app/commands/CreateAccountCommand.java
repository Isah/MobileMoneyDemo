package com.acerat.mm.app.commands;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateAccountCommand {

    @TargetAggregateIdentifier
    private String accountId;
    private double balance;
    private String status;

}
