package com.acerat.mm.app.aggregate;

import com.acerat.mm.app.commands.CreateAccountCommand;
import com.acerat.mm.app.commands.DepositMoneyCommand;
import com.acerat.mm.app.errors.DepositExceptionMaxAmount;
import com.acerat.mm.app.errors.DepositExceptionMinimumAmount;
import com.acerat.mm.app.events.AccountCreatedEvent;
import com.acerat.mm.app.events.AccountDepositEvent;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateCreationPolicy;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.modelling.command.CreationPolicy;
import org.axonframework.spring.stereotype.Aggregate;

@Aggregate
public class AccountAggregate {

    @AggregateIdentifier
    private String accountId;
    private double balance;
    private String status;


    private final double MINIMUM_DEPOSIT_AMOUNT = 500.0;
    private final double MAXIMUM_TRANSACTION_AMOUNT = 5000000.0;

    public AccountAggregate(){

    }

    @CommandHandler
    public AccountAggregate(CreateAccountCommand createAccountCommand){
        AccountCreatedEvent createAccountEvent = AccountCreatedEvent.builder()
                .accountId(createAccountCommand.getAccountId())
                .status(createAccountCommand.getStatus())
                .balance(createAccountCommand.getBalance())
                .build();
        AggregateLifecycle.apply(createAccountEvent);
    }

    //Handle Event for this cmd
    @EventSourcingHandler
    public void on(AccountCreatedEvent createAccountEvent){
        this.accountId = createAccountEvent.getAccountId();
        this.balance = createAccountEvent.getBalance();
        this.status = "CREATED";

        /*
        // Just activate as account gets created
        AccountActivatedEvent activatedEvent = AccountActivatedEvent.builder()
                .accountId(this.accountId)
                .status("ACTIVATED")
                .build();
        AggregateLifecycle.apply(createAccountEvent);
         */
    }

    /*
    //aCCOUNT ACTIVATED EVENT
    @EventSourcingHandler
    public void on(AccountActivatedEvent activatedEvent){;
        this.status = activatedEvent.getStatus();
    }
     */



    // Deposit
    // Command Handler sends the created event (from the cmd ) to the event bus
    // The event is later dispatched to find expected handlers and thanks to the event store
    @CommandHandler   //set the command event to the event bus
    //@CreationPolicy(AggregateCreationPolicy.ALWAYS)
    public void on(DepositMoneyCommand depositMoneyCommand){

        if(depositMoneyCommand.getAmount() < MINIMUM_DEPOSIT_AMOUNT){
            throw new DepositExceptionMinimumAmount("Funds Insufficient 500 is minimum");
        }

        if(depositMoneyCommand.getAmount() > MAXIMUM_TRANSACTION_AMOUNT){
            throw new DepositExceptionMaxAmount("Exceeded transaction amount 5M");
        }


        AccountDepositEvent depositEvent = AccountDepositEvent.builder()
                .accountId(depositMoneyCommand.getAccountId())
                .amount(depositMoneyCommand.getAmount())
                .build();
        AggregateLifecycle.apply(depositEvent);
    }


    @EventSourcingHandler //update the status of this object
    public void on(AccountDepositEvent depositEvent){
        //Deposit money usecase
        System.out.println("Account Deposit Event Sourced 1: ");
        this.accountId = depositEvent.getAccountId();
        System.out.println("Account Deposit Event Sourced 2: "+depositEvent.getAmount());
        // log.info("An AccountCreditedEvent occurred.");
        this.balance += depositEvent.getAmount();
        // this.balance = this.balance.add(accountCreditedEvent.getAmount());
        System.out.println("Account Deposit Event Sourced 3 : "+this.balance);

    }



    /*
    @ExceptionHandler
    public void handleDepositExceptions(DepositMoneyCommand command) {
        // Handles all exceptions thrown from the IssueCardCommand handler within this component
        System.out.println("Ex: "+command.getAmount());
    }

     */


}
