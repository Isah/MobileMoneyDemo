package com.acerat.mm.app.controllers;

import com.acerat.mm.app.commands.CreateAccountCommand;
import com.acerat.mm.app.commands.DepositMoneyCommand;
import com.acerat.mm.app.errors.ApiError;
import com.acerat.mm.app.service.CreateAccountRequest;
import com.acerat.mm.app.service.DepositRequest;
import com.acerat.mm.app.service.AccountCommandService;
import lombok.RequiredArgsConstructor;
import org.axonframework.commandhandling.CommandExecutionException;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@RequiredArgsConstructor
@RestController
@RequestMapping("/accounts")
public class AccountController {

    private final AccountCommandService accountCommandService;

    @Autowired
    private final CommandGateway commandGateway;


    @PostMapping("/create")
    public CompletableFuture<ResponseEntity<String>> createAccount(@RequestBody CreateAccountRequest accountRequest){

        CreateAccountCommand createAccountCommand = new CreateAccountCommand();
        createAccountCommand.setAccountId(UUID.randomUUID().toString());
        createAccountCommand.setStatus(accountRequest.getStatus());
        createAccountCommand.setBalance(0.0);

        return commandGateway.send(createAccountCommand)
                .thenApply(it -> ResponseEntity.ok(""))
                .exceptionally(e -> {
                    String errorResponse = getErrorResponseMessage(e.getCause());
                    // This is also the place where this error can be handled, the command retried etc
                    // or a different logical flow can be attempted
                    // or at least map to frontend friendly error codes / messages.
                    return ResponseEntity.badRequest().body(errorResponse);
                });

    }

    @PostMapping("/deposit")
    public CompletableFuture<ResponseEntity<String>> deposit(@RequestBody DepositRequest depositRequest){

        DepositMoneyCommand depositMoney = new DepositMoneyCommand();
        depositMoney.setAccountId(depositRequest.getAccountId());
        depositMoney.setAmount(depositRequest.getAmount());
        commandGateway.send(depositMoney);

        return commandGateway.send(depositMoney)
                .thenApply(it -> ResponseEntity.ok(""))
                .exceptionally(e -> {
                    String errorResponse = getErrorResponseMessage(e.getCause());
                    // This is also the place where this error can be handled, the command retried etc
                    // or a different logical flow can be attempted
                    // or at least map to frontend friendly error codes / messages.
                    return ResponseEntity.badRequest().body(errorResponse);
                });

    }


    private String getErrorResponseMessage(Throwable throwable) {
        if (throwable instanceof CommandExecutionException) {
            CommandExecutionException cee = (CommandExecutionException) throwable;
            return cee.getDetails()

                    .map((Object it) -> {
                        ApiError error = (ApiError) it;
                        return "" + error.getCode() + " : " + error.getName();
                    })
                    .orElseGet(() -> {

                        return cee.getMessage();
                    });
        }
        else {

            return throwable.getMessage();
        }
    }

}
