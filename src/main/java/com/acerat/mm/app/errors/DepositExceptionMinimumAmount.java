package com.acerat.mm.app.errors;

public class DepositExceptionMinimumAmount extends DepositException{

        public DepositExceptionMinimumAmount() {
            super("Insufficient amount", ErrorCode.INSUFFICIENT_FUNDS);
        }
        public DepositExceptionMinimumAmount(String errorMessage) {
            super(errorMessage, ErrorCode.INSUFFICIENT_FUNDS);
        }
    }
