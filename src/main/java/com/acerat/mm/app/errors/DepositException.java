package com.acerat.mm.app.errors;

public class DepositException extends RuntimeException{

    private String errorMessage;
    private ErrorCode errorCode;

    public DepositException(String errorMessage, ErrorCode errorCode) {
        super(errorMessage);
        this.errorMessage = errorMessage;
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }












}
