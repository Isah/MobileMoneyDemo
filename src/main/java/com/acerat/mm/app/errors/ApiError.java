package com.acerat.mm.app.errors;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sun.istack.NotNull;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class ApiError {

    private final String name;
    @NotNull
    private final ErrorCode code;
    private final String message;

    public ApiError(String name, @NotNull ErrorCode code, String message) {
        this.name = name;
        this.code = code;
        this.message = message;
    }

    public String getName() {
        return name;
    }

    @NotNull
    public ErrorCode getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}