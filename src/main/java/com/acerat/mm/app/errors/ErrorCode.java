package com.acerat.mm.app.errors;

public enum ErrorCode {
    EXCEEDED_FUNDS,
    INSUFFICIENT_FUNDS,
    UNKNOWN
}