package com.acerat.mm.app.errors;

public class DepositExceptionMaxAmount extends DepositException{

        public DepositExceptionMaxAmount() {
            super("Exceeded deposit amount", ErrorCode.EXCEEDED_FUNDS);
        }
        public DepositExceptionMaxAmount(String errorMessage) {
            super(errorMessage, ErrorCode.EXCEEDED_FUNDS);
        }
    }