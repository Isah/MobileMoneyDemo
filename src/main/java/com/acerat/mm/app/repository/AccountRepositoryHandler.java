package com.acerat.mm.app.repository;

import com.acerat.mm.app.events.AccountCreatedEvent;
import com.acerat.mm.app.events.AccountDepositEvent;
import com.acerat.mm.app.repository.Account;
import com.acerat.mm.app.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class AccountRepositoryHandler {

    private final AccountRepository accountRepository;

    @EventHandler
    public void on(AccountCreatedEvent accountCreatedEvent){
        System.out.println("Account Created event: "+accountCreatedEvent.getAccountId());
        Account account = new Account();
        BeanUtils.copyProperties(accountCreatedEvent,account);
        accountRepository.save(account);
    }

    @EventHandler
    public void on(AccountDepositEvent depositEvent){
        System.out.println("DepositEvent called");

        Optional<Account> account = accountRepository.findById(depositEvent.getAccountId());
        if(account.isPresent()){
            // UPDATE
            System.out.println("DepositEvent called: "+account.get().getAccountId());
            Account acc = account.get();
            double oldBalance = acc.getBalance();
            double receivedAmount = depositEvent.getAmount();
            double newBalance = oldBalance + receivedAmount;
            acc.setBalance(newBalance);
            accountRepository.save(acc);
            System.out.println("DepositEvent saved: "+acc.getAccountId());
        }else {
            System.out.println("DepositEvent Account Empty: ");
        }

    }

}
