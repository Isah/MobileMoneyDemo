package com.acerat.mm.app.repository;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Account {

    @Id
    private String accountId;
    private double balance;
    private String status;

}
