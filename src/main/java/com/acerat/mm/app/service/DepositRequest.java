package com.acerat.mm.app.service;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DepositRequest {

    private String accountId;
    private double amount;

}
