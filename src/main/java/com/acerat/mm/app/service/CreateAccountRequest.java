package com.acerat.mm.app.service;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreateAccountRequest {

    private String accountId;
    private double startingbalance;
    private String status;

}
