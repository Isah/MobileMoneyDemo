package com.acerat.mm.app.service;

import com.acerat.mm.app.commands.CreateAccountCommand;
import com.acerat.mm.app.commands.DepositMoneyCommand;
import lombok.RequiredArgsConstructor;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@RequiredArgsConstructor
@Service
public class AccountCommandService {

    private final CommandGateway commandGateway;

    public CompletableFuture<String> createAccount(CreateAccountRequest createAccountRequest){

        CreateAccountCommand createAccountCommand = new CreateAccountCommand();
        createAccountCommand.setAccountId(UUID.randomUUID().toString());
        createAccountCommand.setStatus(createAccountRequest.getStatus());
        createAccountCommand.setBalance(0.0);

       return commandGateway.send(createAccountCommand);

    }


    public CompletableFuture<String> depositToAccount(DepositRequest depositRequest){
        System.out.println("Account  To Deposit: "+depositRequest.getAmount());

        DepositMoneyCommand depositMoney = new DepositMoneyCommand();
        depositMoney.setAccountId(depositRequest.getAccountId());
        depositMoney.setAmount(depositRequest.getAmount());

       return commandGateway.send(depositMoney);

    }


}
