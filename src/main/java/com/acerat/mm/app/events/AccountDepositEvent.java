package com.acerat.mm.app.events;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.axonframework.modelling.command.AggregateIdentifier;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountDepositEvent {

    private String accountId;
    private double amount;
}
